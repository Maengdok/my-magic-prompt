#! /usr/bin/bash

get_help() {
	echo -e "Available commands :
${bold}about${normal}				gives informations about the program
${bold}age${normal}				ask for the user age to know if the user is an adult or not
${bold}cd${normal} [DIRECTORY )			changes the directory
${bold}help${normal} 				access the program's commands
${bold}hour${normal}				prints the actual time in 24 hours style. HH:MM
${bold}httpget${normal} URL [FILE]		gets the source code of a given url and saves it inside a given file. If that file does not exists, it creates it
${bold}ls${normal}				list directory contents
${bold}open${normal} FILE 			opens a given file with vim. If that file does not exists, it creates it
${bold}passw${normal}				changes the user password
${bold}profile${normal}				shows the developper's informations
${bold}pwd${normal}				prints the name of the current directory
${bold}quit${normal}				quit the program and returns 1
${bold}rm${normal} FILE 			removes a file
${bold}rmd${normal} DIRECTORY			removes an empty directory
${bold}rmdirwtf${normal}			allows to delete directories
${bold}rps${normal}				allows 2 players to face each other in a \"Rock, Paper, Scissors\" game
${bold}smtp${normal}				sends an email to a given mail with the given subject and body message
${bold}version | vers | -v${normal}		gives the actual version of the program"
}

delete() {
	command=""
	[[ "${args[0]}" == "rmd" ]] && command="rmdir" || command="rm"
	[[ ( ! "./${args[1]}" == "./main.sh" ) ]] && $command "./${args[1]}" || echo "Denied !"
}

about () {
	echo -e "A prompt script that allows the user to uses shell commands\n"
}

get_version() {
	echo -e "Version 1.16.4\n"
}

age() {
	read -p "How old are you ? " age
	regex='^[0-9]+$'

	[[ $age =~ $regex ]] && ( [[ $age -ge 18 ]] && echo -e "You are an adult !" || echo -e "You are still a kid..." ) || echo -e "That's not a number"
}

profile() {
	echo -e "${bold}First Name${normal} : Axel\n
${bold}Last Name${normal} : Pion\n
${bold}Age${normal} : 29 years old\n
${bold}Email${normal} : pionaxel@gmail.com\n"
}

passw() {
	echo -n "Enter your actual password : "
	read -s actual_password
	echo -e ""

	if [[ "$actual_password" == "$password" ]]
		then
			echo -n "Enter your new password : "
			read -s new_password
			echo -e ""
			echo -n "Repeat your new password :"
			read -s repeat_password
			echo -e ""
			[[ "$new_password" == "$repeat_password" ]] && password=$new_password || echo -e "Not identical" && clear
		else
			echo -e "Password does not match"
	fi
}

httpget() {
	read -p "In what file do you want it to be copied : " fileToCopy
	read -p "What is the address of the website you wish to copy : " link
	wget $link -O $fileToCopy
}

smtp() {
	read -p "To : " sendTo
	read -p "Subject : " subject
	read -p "Message : " message
	echo "${message}" | mail -s "${subject}" $sendTo
}

rps() {
	echo -e "Welcome into the Rock Paper Scissors game\n"
	read -p "Enter first player's name : " fstplayer
	read -p "Enter second player's name : " scndplayer

	fstplayer_points=0
	scndplayer_points=0
	turn=1
	rps="[1] Rock - [2] Paper - [3] Scissors"

	while [ 0 ]
		do
			case "$turn" in
				1 )
					clear
					echo -e "${fstplayer_points} points | ${scndplayer} : ${scndplayer_points} points\n${fstplayer}'s turn.\nWhat will you choose ?\n${rps}"
					read fstplayer_choice

					case "$fstplayer_choice" in
						[1] | 1 ) fstplayer_choice="Rock" && turn=2;;
						[2] | 2 ) fstplayer_choice="Paper" && turn=2;;
						[3] | 3 ) fstplayer_choice="Scissors" && turn=2;;
						[4] | 4 ) fstplayer_choice="SuperKitty" && turn=2;;
						* ) echo -e "Nope..."
					esac;;
				2 )
					clear
					echo -e "${fstplayer_points} points | ${scndplayer} : ${scndplayer_points} points\n${scndplayer}'s turn.\nWhat will you choose ?\n${rps}"
					read scndplayer_choice

					case "$scndplayer_choice" in
						[1] | 1 ) scndplayer_choice="Rock" && turn=0;;
						[2] | 2 ) scndplayer_choice="Paper" && turn=0;;
						[3] | 3 ) scndplayer_choice="Scissors" && turn=0;;
						[4] | 4 ) scndplayer_choice="SuperKitty" && turn=0;;
						* ) echo -e "Nope..."
					esac;;
				0 )
					fp_wins="${fstplayer_choice} beats the ${scndplayer_choice} !\n${fstplayer} wins the round !"
					sp_wins="${scndplayer_choice} beats the ${fstplayer_choice} !\n${scndplayer} wins the round !"
					draw="${fstplayer_choice} and ${scndplayer_choice} fought well but none of them could win this round"
					fp_thanos="Thanos just clapped its finger and ${scndplayer} disappeared into dust.\n${fstplayer} wins the game !"
					sp_thanos="Thanos just clapped its finger and ${fstplayer} disappeared into dust.\n${scndplayer} wins the game !"
					spfp_thanos="Thanos cannot clap himself, if you keep trying that maneuver you are going to annihilate the universe"

					clear
					echo -e "${fstplayer} sent a ${fstplayer_choice} and ${scndplayer} sent a ${scndplayer_choice} !"

					case "$fstplayer_choice" in
						"Rock" )
							case "$scndplayer_choice" in
								"Rock" ) echo -e $draw && turn=1 && sleep 2;;
								"Paper" ) 
									turn=1 && ((scndplayer_points=scndplayer_points+1)) && echo -e "${sp_wins}\n${fstplayer} : ${fstplayer_points} points | ${scndplayer} : ${scndplayer_points} points" && sleep 2;;
								"Scissors" )
									turn=1 && ((fstplayer_points=fstplayer_points+1)) && echo -e "${fp_wins}\n${fstplayer} : ${fstplayer_points} points | ${scndplayer} : ${scndplayer_points} points" && sleep 2;;
								"SuperKitty" ) echo -e $sp_thanos && sleep 2 && scndplayer_points=3;;
							esac;;
						"Paper" )
							case "$scndplayer_choice" in
								"Rock" )
									turn=1 && ((fstplayer_points=fstplayer_points+1)) && echo -e "${fp_wins}\n${fstplayer} : ${fstplayer_points} points | ${scndplayer} : ${scndplayer_points} points" && sleep 2;;
								"Paper" ) echo -e $draw && turn=1 && sleep 2;;
								"Scissors" ) 
									turn=1 && ((scndplayer_points=scndplayer_points+1)) && echo -e "${sp_wins}\n${fstplayer} : ${fstplayer_points} points | ${scndplayer} : ${scndplayer_points} points" && sleep 2;;
								"SuperKitty" ) echo -e $sp_thanos && sleep 2 && scndplayer_points=3;;
							esac;;
						"Scissors" )
							case "$scndplayer_choice" in
								"Rock" )
									turn=1 && ((fstplayer_points=fstplayer_points+1)) && echo -e "${fp_wins}\n${fstplayer} : ${fstplayer_points} points | ${scndplayer} : ${scndplayer_points} points" && sleep 2;;
								"Paper" )
									turn=1 && ((scndplayer_points=scndplayer_points+1)) && echo -e "${sp_wins}\n${fstplayer} : ${fstplayer_points} points | ${scndplayer} : ${scndplayer_points} points" && sleep 2;;
								"Scissors" ) echo -e $draw && turn=1 && sleep 2;;
								"SuperKitty" ) echo -e $sp_thanos && sleep 2 && scndplayer_points=3;;
							esac;;
							"SuperKitty" )
							case "$scndplayer_choice" in
								"Rock" ) echo -e $fp_thanos && sleep 2 && scndplayer_points=3;;
								"Paper" ) echo -e $fp_thanos && sleep 2 && scndplayer_points=3;;
								"Scissors" ) echo -e $fp_thanos && sleep 2 && scndplayer_points=3;;
								"SuperKitty" ) echo -e $spfp_thanos && sleep 2 && fstplayer_points=3 && scndplayer_points=3;;
							esac;;
					esac
					[[ ( "$fstplayer_points" -eq 3 ) || ( "$scndplayer_points" -eq 3 ) ]]	&& prompt || "";;
				esac
		done
}

rmdirwtf() {
	unset "args[0]"
	tab=( "${args[*]}" )

	echo -n "Enter your password : "
	read -s rm_password
	clear

	if [[ "$rm_password" == "$password" ]]
		then
			for value in ${tab[@]}
				do
					rmdir "./$value"
				done
		else
			clear
			echo -e "Wrong password. Try again"
			sleep 2
			clear
			rmdirwtf $tab
	fi
}

prompt() {
	while [ true ];
		do
			read -p "Enter your command : " command
			args=( $command )
			clear

			case "${args[0]}" in
				about ) about;;
				age ) age;;
				cd ) cd ${args[1]};;
				help ) get_help;;
				hour ) date +"%R";;
				httpget ) httpget;;
				ls ) ls -a;;
				open ) vim "${args[1]}";;
				passw ) passw;;
				profile ) profile;;
				pwd ) pwd;;
				quit ) clear && exit 1;;
				rm ) delete ${args[*]};;
				rmd ) delete "${args[*]}";;
				rmdirwtf ) rmdirwtf "${args[*]}";;
				rps ) rps;;
				smtp ) smtp;;
				version | vers | --v ) get_version;;
				* ) echo -e "Unknown command\n";;
			esac
		done	
}

main() {
	bold=$(tput bold)
	normal=$(tput sgr0)

	clear
	read -p "Enter your login : " user
	echo -n "Enter your password : "
	read -s password

	if [[ ( "$user" == "Maengdok" ) && ( "$password" == "abc" ) ]]
		then
			clear
			prompt
	else
		clear
		echo -e "Wrong user.\nClosing in 2 secondes"
		sleep 2
		clear
		exit 1
	fi
}

main