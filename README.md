# my-magic-prompt

### Version 1.16.4

**A MyDigitalSchool B3 Unix Shell project**

A prompt script that allows the user to uses shell commands

## Available commands

### **help**
    Access the program's commands
___
### **ls**
    List directory hidden or non hidden contents
___
### **rm** FILE
    Removes a file
___
### **rmd** DIRECTORY
    Removes an empty directory
___
### **about**
    Gives informations about the program
___
### **version** | **vers** | **-v**
    Gives the actual version of the program
___
### **age**
    Ask for the user age to know if the user is an adult or not
___
### **quit**
    Quit the program and returns 1
___
### **profile**
    Shows the developper's informations
___
### **passw**
    Changes the user password
___
### **cd** [DIRECTORY]
    Changes the directory
___
### **pwd**
    Prints the name of the current directory
___
### **hour**
    Prints the actual time in 24 hours style. HH:MM
___
### **httpget** URL [FILE]
    Gets the source code of a given url and saves it inside a given file. If that file does not exists, it creates it
___
### **smtp**
    Sends an email to a given mail with the given subject and body message
___
### **open** FILE
    Opens a given file with vim. If that file does not exists, it creates it
___
### **rps**
    Allows 2 players to face each other in a \"Rock, Paper, Scissors\" game."
___
### **rmdirwtf**
    Allows to delete directories
